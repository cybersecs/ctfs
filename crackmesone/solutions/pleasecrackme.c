#include <stdio.h>

int main() {
    char usrname[100], gen_passwd[100];
    int num_input, i;

    printf("Type in your Username: ");
    scanf("%s", usrname);

    printf("Type in a number between 1 and 9: ");
    scanf("%d", &num_input);

    if (num_input < 1 || num_input > 9) {
        puts("\nError: Number is out of range");
        return 1; // Exit with error status
    }

    for (i = 0; usrname[i] != '\0'; i++) {
        gen_passwd[i] = usrname[i] + num_input;
    }
    gen_passwd[i] = '\0'; // Null-terminate the password string

    printf("\nYour generated password is: ");
    printf("%s", gen_passwd);

    return 0; // Exit with success status
}
