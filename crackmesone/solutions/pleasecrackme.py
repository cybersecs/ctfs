def solver(usrname, num_input):
    if num_input < 1 or num_input > 9:
        print("\nError: Number is out of range")
        exit()

    # Generate password
    gen_passwd = ''.join(chr(ord(c) + num_input) for c in usrname)
    print(f"\nGenerated Password: {gen_passwd}")


if __name__ == "__main__":
    usrname = input("Type in your Username: ")
    num_input = int(input("Type in a number between 1 and 9: "))

    solver(usrname, num_input)

