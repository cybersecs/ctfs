[Back](../README.md)

#### [RaphDev's PleaseCrackMe](https://crackmes.one/crackme/612e85d833c5d41acedffa4f)
- [python solution](solutions/pleasecrackme.py)
- [c solution](solutions/pleasecrackme.c)
