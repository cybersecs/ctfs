---
aliases:
  - TryHackMe CTF Like Room Notes
---

[Back](../THMMachines.md)

>[!note] TOC/Links:
> - [Fowsniff CTF machine](#fowsniff_ctf)/[Fowsniff CTF](https://tryhackme.com/r/room/ctf)
> - [c4c4ptur3-th3-fl4g](#c4c4ptur3-th3-fl4g)/[c4c4ptur3-th3-fl4g](https://tryhackme.com/r/room/c4ptur3th3fl4g)
> - [Cheese CTF](#cheesectf)/[Cheese CTF](https://tryhackme.com/r/room/cheesectfv10)
> - [RootMe](#rootme)/[RootMe][](https://tryhackme.com/room/rrootme)
> - [Wgel CTF](#wgel_ctf)/[Wgel CTF](https://tryhackme.com/r/room/wgelctf)

## fowsniff_ctf

The most commonly used commands in a POP3 connection are as follows:
```bash
USER <username>
PASS <password>
STAT
LIST
RETR
DELE
RSET
TOP
QUIT
```

>[!note] Metasploit pop3 credentials brute force
> create files with usernames/passwords (one on each line)
> msf > use auxiliary/scanner/pop3/pop3_login
> msf auxiliary(scanner/pop3/pop3_login) > set rhosts 192.168.1.29
> msf auxiliary(scanner/pop3/pop3_login) > set user_file user.txt
> msf auxiliary(scanner/pop3/pop3_login) > set pass_file pass.txt
> msf auxiliary(scanner/pop3/pop3_login) > set verbose false
> msf auxiliary(scanner/pop3/pop3_login) > run

[Reverse Shell Cheat Sheet \| pentestmonkey](https://pentestmonkey.net/cheat-sheet/shells/reverse-shell-cheat-sheet)

>[!hint] Walkthrough:
> [Fowsniff: 1 Vulnhub Walkthrough - Hacking Articles](https://www.hackingarticles.in/fowsniff-1-vulnhub-walkthrough/)

## c4c4ptur3-th3-fl4g

>[!hint] Encodings used in this challenge:
> binary, base32/64, hexadecimal (or base16), rot13/47, morse, BCD (Binary Coded Decimal)

### Spectrograms:

Tools to choose from:
- [Sonic Visualizer](https://www.sonicvisualiser.org/download.html)
- [DCode Spectral Analysis](https://www.dcode.fr/spectral-analysis)
- [Audacity Spectrogram View](https://manual.audacityteam.org/man/spectrogram_view.html)

### Steganography:

#### Various Steganography tools
1. [Steghide](https://steghide.sourceforge.net/)
	- `steghide extract -sf stegosteg.jpg`
	- [Advanced Steganography methods on steghide \| by Premkumar S \| Medium](https://medium.com/@prem112/advanced-steganography-methods-on-steghide-d337edfdccc8)
2. [Aperi'Solve](https://www.aperisolve.com/)
	- platform also uses zsteg, steghide, outguess, exiftool, binwalk, foremost and strings for deeper steganography analysis
3. OpenStego
4. SilentEye
5. ImageMagick

## cheesectf

- `nmap` -> see insanity and add `--reason` flag
- it can be [GitHub - drk1wi/portspoof: Portspoof](https://github.com/drk1wi/portspoof)

>[!info] PHP Schema
> [LFI to RCE using PHP Filters! - Tib3rius](https://www.youtube.com/watch?v=PVdOSpF4Tl0)
> [PHP Wrappers](https://www.php.net/manual/en/wrappers) -> `php://filter`
> [PHP Filters Chain Generator](https://github.com/synacktiv/php_filter_chain_generator)
> [PHP Filters Chain - Exploit Notes](https://exploit-notes.hdks.org/exploit/web/security-risk/php-filters-chain/)
> [LFI2RCE via PHP Filters - HackTricks](https://book.hacktricks.wiki/en/pentesting-web/file-inclusion/lfi2rce-via-php-filters.html)

After all this PHP madness:
- check `.ssh` permissions
- ssh to target and `sudo -l` to `exploit.service`
- [gtfobins xxd](https://gtfobins.github.io/gtfobins/xxd/)

## rootme

>[!bug] File upload vulnerability

- enumerate the machine
- upload php reverse shell
- change file extension -> [Upload Insecure Files - PayloadsAllTheThings](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Upload%20Insecure%20Files/README.md#defaults-extensions)
- connect and search for files with SUID permissions - `find / -perm -u=s -type f 2>/dev/null`
- find exploits -> [gtfobins](https://gtfobins.github.io/)

## wgel_ctf

- <!- **Jessie** don't forget to udate the webste --> username for ssh
- fuzz directories (recursively)
- ssh to target and `sudo -l` to get available binaries
- go to gtfobins and choose: just upload file or get root privileges
- don't forget to have fun with laggy tryhackme service










