def getCorrectSets():
    wordSets = {
        1: ["Tinsel", "Sleigh", "Belafonte", "Bag", "Comet", "Garland", "Jingle Bells", "Mittens", "Vixen", "Gifts", "Star", "Crosby", "White Christmas", "Prancer", "Lights", "Blitzen"],
        2: ["Nmap", "burp", "Frida", "OWASP Zap", "Metasploit", "netcat", "Cycript", "Nikto", "Cobalt Strike", "wfuzz", "Wireshark", "AppMon", "apktool", "HAVOC", "Nessus", "Empire"],
        3: ["AES", "WEP", "Symmetric", "WPA2", "Caesar", "RSA", "Asymmetric", "TKIP", "One-time Pad", "LEAP", "Blowfish", "hash", "hybrid", "Ottendorf", "3DES", "Scytale"],
        4: ["IGMP", "TLS", "Ethernet", "SSL", "HTTP", "IPX", "PPP", "IPSec", "FTP", "SSH", "IP", "IEEE 802.11", "ARP", "SMTP", "ICMP", "DNS"]
    };


    correctSets = [
        [0, 5, 10, 14],
        [1, 3, 7, 9],
        [2, 6, 11, 12],
        [4, 8, 13, 15]
    ];

    return [[[wordSets[set_num + 1][i] for i in indices] for indices in correctSets] for set_num in range(len(wordSets))]

if __name__ == "__main__":
    correct_lists = getCorrectSets()
    for i, word_sets in enumerate(correct_lists, start=1):
        print(f"Word Set {i}: {word_sets}\n")


