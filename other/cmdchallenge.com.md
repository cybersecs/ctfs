---
tags:
  - "#cybersecurity/training/cmdchallenge"
  - "#ctf/cmdchallenge"
medium: cmdchallenge
status: in-progress
created date: 27.08.24
related: bash
aliases:
  - Command Challenge Notes
---

# Command Challenge

- List names of all the files in the current directory, *one file per line*.

```bash
ls -1A
# -1 - list one file per line
# -A, --almost-all - do not list implied . and ..
for f in *; do echo $f; done
```

- Print the *last 5* lines of "access.log".

```bash
sed -n '6,10p' access.log # will print lines 6 through 10 from the `access.log` file to the terminal.
awk -v start_line=$(( $(wc -l < llama.log) - 4 )) 'NR >= start_line' llama.log
tail -n 5 access.log
```


- Delete all of the files in this challenge directory including all subdirectories and their contents.

>[!hint] There are files and directories that start with a dot ".", "rm -rf *" won't work here!

```bash
find . -delete
rm -rf $(ls -a)
rm -rf * .*
find . | xargs rm -rf
```

- Print all files in the current directory, one per line (not the path, just the filename) that contain the string "500".

```bash
grep -lr 500
# -r, --recursive - Read all files under each directory, recursively, following symbolic links only if they are on the command line. 
# -l, --files-with-matches - Suppress  normal output; instead print the name of each input file from which output would normally have been printed. Scanning each input file stops upon first match.
```

- Print all matching lines (without the filename or the file path) in all files under the current directory that start with "access.log" that contain the string "500".
```bash
grep -r -h '500'
```

>[!note] -h, --no-filename
> Suppress  the  prefixing  of  file names on output.  This is the default when there is only one file (or only standard input) to search.

- Extract all IP addresses from files that start with "access.log" printing one IP address per line.
```bash
grep -r . | awk '{print $1}'
cat **/access.log* | awk '{print $1}'
awk '{print $1}' **/access.log*
grep -ro ^[*-9]* #  -o, --only-matching 
```

- Count the number of files in the current working directory. Print the number of files as a single integer.

```bash
ls -l / -A | wc -l
find . -type f | wc -l
```

- Print the contents of access.log sorted.

```bash
sort access.log
```

- Print the numbers 1 to 100 separated by spaces.

```bash
for f in {1..100}; do echo -n "$f "; done
# -n - Print a message without the trailing newline
echo -n {1..100}
seq -s ' ' 1 100
# -s - Separate the output with a space instead of a newline:

```