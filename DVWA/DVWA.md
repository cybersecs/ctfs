[Back](../README.md)

# DVWA

## [DVWA: Web Application Security](https://github.com/digininja/DVWA)

### Setting up inside Kali VM

```bash
git clone https://github.com/digininja/DVWA.git
mv DVWA /var/www/html/
sudo service apache2 start
cp config/config.inc.php.dist config/config.inc.php
```

- go to `localhost/DVWA/setup.php`
- run mariadb server `service mariadb start`

```bash
create database dvwa;
create user dvwa@localhost identified by 'p@ssw0rd';
grant all on dvwa.* to dvwa@localhost;
flush privileges;
```

>[!note] In DVWA Security change level to those that match your skills

### Cleaning up the red lines

- in `/etc/php/<version>/apache2/php.ini` enable functions
- install `php-gd`
- make folders writable by `chown www-data /var/www/html/DVWA/{config,hackable/uploads}` (or `chmod 777 /var/www/html/DVWA/{config,hackable/uploads}`)

>[!note] Restart apache2

