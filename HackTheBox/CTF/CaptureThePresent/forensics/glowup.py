import base64

def xor_bytes(data, key):
    return bytes(a ^ key for a in data)

def decode_base64_and_xor(encoded_data, key):
    decoded_data = base64.b64decode(encoded_data)
    xored_data = xor_bytes(decoded_data, key)
    print("Xored data: ", xored_data)
    return xored_data

urls = [
    "loqKjo3E0dGTl4yfl5mMkYuO0JaKnNGJjtOXkJ2Si5qbjdGJlZ2Jx87MzsvR",
    "loqKjo3E0dGZm5HTl4rQloqc0Z2NjdGPzYTNkpSRzcfK0baqvIXOnJiLjZ3Kis+QmaGKls2hrYqMz5CZjaHPjaGQzoqhzZDOi5mWgw==",
    "loqKjo3E0dGdn4ybm4zQloqc0YmO052RkIqbkIrRzpCEjo6Gj8rH0Q==",
    "loqKjo3E0dGZm5HTl4rQloqc0Z2NjdGPzYTNkpSRzcfK0Q==",
    "loqKjsTR0YmJidCdnJCQm4mNmpeMm52K0JaKnNGJkYyajoybjY3Ry5LPlY6GysvR",
    "loqKjo3E0dGVn4yHn4qWn5LQloqc0YmO052RkIqbkIrRzM3P0Q=="
]

for url in urls:
    decoded_data = decode_base64_and_xor(url, 254)
