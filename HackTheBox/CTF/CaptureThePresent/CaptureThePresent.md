[Back](../../README.md)

# CaptureThePresent

## Forensics

### GlowUp

>[!note]
> When you see a hash or cipher that ends with ==, it is typically a Base64-encoded string.
> Base64 encoding is commonly used to represent binary data in an ASCII string format.
> The == at the end of the string is used for padding to ensure that the encoded string has a length that is a multiple of 4 characters.

- [decoded urls](forensics/glowup.py)

## Reversing

### Spelunking

>[!hint] Ghidra
> Flag in main function.

### Unpacking

>[!hint]
> Unpack with suggested tool
> [UPX](https://github.com/upx/upx)
> `string`

