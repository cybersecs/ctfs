---
aliases:
  - HTB Season 7 Machines List
---

[Back](../HTBMachines.md)

#### Table of Contents:
- [EscapeTwo](#EscapeTwo)/[HTB Link](https://app.hackthebox.com/machines/642)

# Machines

## EscapeTwo
- scan target
- LDAP enum
- smb enum (`netexec` and `smbclient`)
- connect to share and get `*.xlsx` files
- extract `*.xlsx` files with `unzip` or online - [XLSX Viewer - Open XLSX File Online for Free \| Jumpshare](https://jumpshare.com/viewer/xlsx)
- and continue this [amaizing walkthrough](https://bloodstiller.com/walkthroughs/escapetwo-box/yy) after finishing AD and LDAP modules + few windows machines



