from functools import wraps
import re
import sys

def extract_flag(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        # Call the original function and get the response
        resp = func(*args, **kwargs)

        try:
            # Define the regex pattern to match the flag
            pattern = r"HTB\{[^}]+\}"

            # Search for the pattern in the response text
            match = re.search(pattern, resp.text)

            if match:
                flag = match.group(0)
                print(f"[SUCCESS] Flag extracted: {flag}")
            else:
                print("[ERROR] Flag not found in the response.")
        except Exception as e:
            print(f"[ERROR] An error occurred while extracting the flag: {e}")
            sys.exit(1)

        return resp  # Return the original response for further processing if needed

    return wrapper
