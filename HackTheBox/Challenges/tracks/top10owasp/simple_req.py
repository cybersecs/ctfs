#!/usr/bin/env python3
import requests, sys
from dec_flag_match import extract_flag

@extract_flag
def main(url):
    req_data = {"name": "pepe"}
    resp = requests.post(url, data=req_data)
    return resp

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python3 simple_req.py <host:port>")
        sys.exit(1)
    HOST, PORT = sys.argv[1].split(":")
    CHALLENGE_URL = f"http://{HOST}:{PORT}"
    main(CHALLENGE_URL)
