def julius_decrypt(msg, shift):
    pt = ''
    for p in msg:
        if p == '0':
            pt += ' '
        elif not ord('A') <= ord(p) <= ord('Z'):
            pt += p
        else:
            o = ord(p) - 65
            pt += chr(65 + (o - shift) % 26)
    return pt

def decrypt(msg, key):
    for shift in key:
        msg = julius_decrypt(msg, shift)
    return msg

msg = open('output.txt').read()

# Function to display the next 5 decrypted values with incremental shifts
def display_decryptions(m, start_shift):
    for shift in range(start_shift, start_shift + 5):
        print(f"\nShift: {shift}")
        decrypted_message = julius_decrypt(m, shift)
        print(decrypted_message)

current_shift = 0
with open('output.txt', 'r') as f:
    msg = f.read()

    while True:
        input("Press Enter to show next 5 shift values...")
        display_decryptions(msg, current_shift)
        current_shift += 5  # Increment by 5 for the next set of shifts

