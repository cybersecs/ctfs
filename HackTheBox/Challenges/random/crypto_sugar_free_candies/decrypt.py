from Crypto.Util.number import long_to_bytes
from sympy import symbols, Eq, solve


with open('output.txt', 'r') as f:
    v1 = int(f.readline().split(" = ")[1])
    v2 = int(f.readline().split(" = ")[1])
    v3 = int(f.readline().split(" = ")[1])
    v4 = int(f.readline().split(" = ")[1])

    x, y, z = symbols('x y z')
    eq1 = Eq(x**3 + z**2 + y, v1)
    eq2 = Eq(y**3 + x**2 + z, v2)
    eq3 = Eq(z**3 + y**2 + x, v3)
    eq4 = Eq(x + y + z, v4)

    solutions = list(solve((eq1, eq2, eq3, eq4), (x, y, z))[0])

    flag = b''
    for i in range(len(solutions)):
        flag += long_to_bytes(solutions[i])

    print(f"Decrypted flag: {flag.decode('utf-8', errors='ignore')}")

