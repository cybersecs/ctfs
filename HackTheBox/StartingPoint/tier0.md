---
aliases:
- HackTheBox Tier 0 Machines Notes
---
[Back](../HTBMachines.md)

# Table of Contents:

- [Meow](#meow)
- [Fawn](#fawn)
- [Redeemer](#redeemer)
- [Explosion](#explosion)
- [Preignition](#preignition)
- [Mongod](#mongod)
- [Synced](#synced)


# Meow

##### What service do we identify on port 23/tcp during our scans?

Scan open ports:

```bash
nmap -sT -p 23 <target_ip>
# `-sT` option specifies the scan type, which in this case is a TCP connect scan
# `-p 23` option specifies the port to scan.
```

```bash
nmap -sT -p 1-65535 <target_ip>
# This will scan all 65,535 TCP ports on the target IP.
```

Connect to a Telnet server:

```bash
telnet <target_ip> 23
```

# Fawn

##### From your scans, what version is FTP running on the target?

`sudo nmap -sS -sV 10.129.117.38`

##### What is username that is used over FTP when you want to log in without having an account?

- try using the "anonymous" username.
- This is a common convention for FTP servers to allow anonymous access.

##### What is the command used to download the file we found on the FTP server?

```
!               edit            lpage           nlist           rcvbuf          struct
$               epsv            lpwd            nmap            recv            sunique
account         epsv4           ls              ntrans          reget           system
append          epsv6           macdef          open            remopts         tenex
ascii           exit            mdelete         page            rename          throttle
bell            features        mdir            passive         reset           trace
binary          fget            mget            pdir            restart         type
bye             form            mkdir           pls             rhelp           umask
case            ftp             mls             pmlsd           rmdir           unset
cd              gate            mlsd            preserve        rstatus         usage
cdup            get             mlst            progress        runique         user
chmod           glob            mode            prompt          send            verbose
close           hash            modtime         proxy           sendport        xferbuf
cr              help            more            put             set             ?
debug           idle            mput            pwd             site
delete          image           mreget          quit            size
dir             lcd             msend           quote           sndbuf
disconnect      less            newer           rate            status
```

**get**

# Dancing

```bash
sudo nmap -sV {target_ip}
# `-sV` switch is used to display the version of the services running on the open ports
```

###### Using smbclient to Connect to the Share

```bash
smbclient \\\\{target_ip}\\{share_name}
```

- **ADMIN$** = These are hidden network shares. These allow system administrators to have remote access to every disk volume on a network-connected system. These shares cannot be permanently deleted but may be disabled.
- **C$ =** The operating system is hosted here.
- **IPC$** = Used for inter-process communication via named pipes and is not part of the file system.

```bash
smbclient -L IPaddressOfTarget -U htb-student
# `-L` is used to list shares -U htb-student
# `-U` is used to specify the username
# `-P` is used to specify the password
```

# Redeemer

```bash
nmap -p- —min-rate 5000 -sV target_ip
redis-cli --help
redis-cli -h target_ip
```

### Inside the redis server:

```bash
info
keys *
get <key>
```

# Explosion

- `sudo nmap -n -PN -p- <target_ip>` gives us all the open ports
- `3389/tcp  open  ms-wbt-server` is the RDP port
- `telnet` an old remote access tool that came without encryption by default and listens on TCP port 23

>[!hint] When connecting to RDP, at first try use usernames like: admin, Administrator, root, or user.

# Preignition

```bash
gobuster [command] --help
gobuster dir -x php # finds PHP pages
gobuster dir -x php -w /usr/share/wordlists/dirb/common.txt -u <target_ip>:<port>
```

# Mongod

- nmap ip address
- `mongosh` cli for mongodb

# Synced

- rsync server default port is 873
- `rsync --list-only rsync://<server_address>/<share_name>`
