---
aliases:
  - HTB Machines List
---

[Back](../README.md)

# Machines

>[!todo] Try to write python/bash script to automate it exploitation

## Setup
- variables: $box (ip), $user, $pass, $domain, $machine, $hash


## Starting Point

#### [Tier 0 Machines Notes]('StartingPoint/tier0.md')
#### [Tier 1 Machines Notes]('StartingPoint/tier1.md')
#### [Tier 2 Machines Notes]('StartingPoint/tier2.md')

## Machines List

### [Easy Machines]('Machines/Easy.md')
### [Medium Machines]('Machines/Medium.md')

## Seasons

### [Season 7 Machines]('Machines/Season7.md')

