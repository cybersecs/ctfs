[Back](../README.md)

### Table of Contents
#### Tracks

- [looking_glass](#looking_glass)/[htb](https://app.hackthebox.com/challenges/177)
- [sanitize](#sanitize)/[htb](https://app.hackthebox.com/challenges/178)
- [baby_auth](#baby_auth)/[htb](https://app.hackthebox.com/challenges/179)
- [baby_nginxatsu](#baby_nginxatsu)/[htb](https://app.hackthebox.com/challenges/180)
- [baby WAFfles order](#baby_WAFfles_order)/[htb](https://app.hackthebox.com/challenges/181)
- [baby todo or not todo](#baby_todo_or_not_todo)/[htb](https://app.hackthebox.com/challenges/182)
- [baby BoneChewerCon](#baby_BoneChewerCon)/[htb](https://app.hackthebox.com/challenges/baby%20BoneChewerCon)
- [Full Stack Conf](#full_stack_conf)/[htb](https://app.hackthebox.com/challenges/184)
- [baby_website_rick](#baby_website_rick)/[htb](https://app.hackthebox.com/challenges/185)
- [baby_breaking_grad](#baby_breaking_grad)/[htb](https://app.hackthebox.com/challenges/186)

#### Random Challenges
- [sekur julius](#sekur_julius)
- [Golfer - Part 1](#Golfer_Part_1)
- [sugar_free_candies](#sugar_free_candies)

>[!note] Links:
> [sekur julius](https://app.hackthebox.com/challenges/sekur%20julius)
> [Golfer - Part 1](https://app.hackthebox.com/challenges/golfer---part-1)
> [sugar_free_candies](https://app.hackthebox.com/challenges/sugar-free-candies)

# Tracks

## [OWASP Top 10](https://app.hackthebox.com/tracks/OWASP-Top-10)

### looking_glass
>[!bug] Command Injection
- [Refactored HTB solver](./Challenges/tracks/top10owasp/ci_script.py)

### sanitize
>[!bug] SQL Injection
- [Commented and refactored HTB solver - POST request](./Challenges/tracks/top10owasp/sqli_post.py)

### baby_auth
>[!bug] Broken Authentication
- [Commented and refactored HTB solver](./Challenges/tracks/top10owasp/ba_script.py)

### baby_nginxatsu
>[!bug] Sensitive data exposure
> - leads to leakage of MD5 hashed admin password

1. navigate to `/storage`
2. download and unzip backup file
3. browse via `sqlite3` or *sqlitebrowser*
4. crack hashes on [crackstation](https://crackstation.neat/) or `john`

@todo - crack hashes with john and hashcat

### baby_WAFfles_order
>[!bug] XXE

- change *Content-Type* to `application/xml`
- convert json request body to xml
- find payloads -> get `/flag`

### baby_todo_or_not_todo
>[!bug] Application logic bug
> leads to leakage of sensitive info

- or use `ffuf` to fuzz api endpoints (include `?secret=` and `-H "Cookie: <cookie>"`)
- in html body we found - "// don't use getstatus('all') until we get the verify_integrity() patched"
- url contain `?secret=<secret>`
- requesting `/api/list/all` with our secret we find our flag

- [rewritten htb solver](./Challenges/tracks/top10owasp/todo.py)

### baby_BoneChewerCon

- test the input field
- search for the flag
- or use [simple request script](./Challenges/tracks/top10owasp/simple_req.py)

### full_stack_conf

>[!bug] XSS

- *From description*: "But be very careful with the stay up to date form, we don't sanitize anything and the admin logs in and checks the emails regularly, don't try anything funny!! 😅"
- *From site*: "But be very careful with the stay up to date form, we don't sanitize anything and the admin logs in and checks the emails regularly, don't try anything funny!! 😅"

>[!hint] XSS Payload:
> `<script>alert("XSS")</script>`

### baby_website_rick

>[!bug] Insecure Deserialization - RCE

- `planb` in req/res cookies
- [picklesolver](./Challenges/tracks/top10owasp/pickledeep.py)

### baby_breaking_grad

>[!bug] Code Execution Vulnerability - Prototype Pollution RCE
> - exploiting the `static-eval` library
> - sandbox bypass

>[!note] Whitebox approach
- since we have the source code

- helper library `StudentHelper.js` that contains the core logic
- `static-eval` vulnerabilities found in the library before:
    - [Unsafe Code Execution in static-eval](https://maustin.net/articles/2017-10/static_eval)
    - [Bypassing a restrictive JS sandbox](https://licenciaparahackear.github.io/en/posts/bypassing-a-restrictive-js-sandbox/)
    - [source commit](https://github.com/browserify/static-eval/commit/0bcd9dc93f42898dfd832a10915a4544e11b8f13)

>[!hint] Payload
> from [braincoke write up](https://braincoke.fr/write-up/hack-the-box/baby-breaking-grad/)
```json
{
  "name":"smth",
  "formula": "(function myTag(y){return ''[!y?'__proto__':'constructor'][y]})('constructor')('throw new Error(global.process.mainModule.constructor._load(\"child_process\").execSync(\"cat flag\"))')()"
}
```
> or use [htb solver](./Challenges/tracks/top10owasp/statcevlpollutn.py)


# Random Challenges

## sekur_julius

- [encription source](Challenges/random/crypto_sekur_julius/source.py)
- [decryption](Challenges/random/crypto_sekur_julius/decrypt.py)

## Golfer_Part_1

### radare2
- open in write mode (`-w`)
- analyze: `aaaa`
- list functions: `afl`
- view assembly: `v`
- write `nop` to first *entry* instruction: `wao nop`
- quit and run

## sugar_free_candies

- [encription source](Challenges/random/crypto_sugar_free_candies/source.py)
- [decryption](Challenges/random/crypto_sugar_free_candies/decrypt.py)

