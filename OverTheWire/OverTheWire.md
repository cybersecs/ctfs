---
created date: 23.04.2023
aliases:
- OverTheWire Wargame Notes
---
Links:
- [bandit](https://overthewire.org/wargames/bandit)
- [sshpass](https://www.redhat.com/sysadmin/ssh-automation-sshpass)
___
wiki:
- [hexdump](https://en.wikipedia.org/wiki/Hex_dump)

### Available Tools
https://github.com/Gallopsled/pwntools
https://github.com/longld/peda.git
https://github.com/hugsy/gefcom
https://github.com/hugsy/gef
https://github.com/hugsy/gefgef in /opt/gef/
pwndbg (https://github.com/pwndbg/pwndbg) in /opt/pwndbg/
peda (https://github.com/longld/peda.git) in /opt/peda/
gdbinit (https://github.com/gdbinit/Gdbinit) in /opt/gdbinit/
pwntools (https://github.com/Gallopsled/pwntools)
radare2 (http://www.radare.org/)

## [OverTheWire: Bandit](Bandit.md)
## [OverTheWire: Natas](Natas.md)

