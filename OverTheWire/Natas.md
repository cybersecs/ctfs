[Back](OverTheWire.md)

>[!warning] Try to write automated tool to reach all levels

# Natas

## [OverTheWire: Natas](https://overthewire.org/wargames/natas/)

### level 0
### level 0-1

- in comments -> div id=content

### level 1-2


- in comments inside `div id=content` image sourced from `/files/pixel.png`
- add to address `/files`

### level 2-3

>[!hint] "Not even Google will find it this time..."

- `robots.txt` - instructions for search engines (web crawlers)

### level 3-4
