[Back](OverTheWire.md)

# Bandit

## [OverTheWire: Bandit](https://overthewire.org/wargames/bandit/)

```shell

grep ^ ./-file0*

for i in $(la); do file ./$i; done

##########################################################################
find . -type f -size 1033c ! -executable -printf '%p %s\n' -exec cat {} \;
# or
cat $(find . -type f -size 1033c ! -executable) | tr -d " \t\r\n"
# or
(find . -readable -size 1033c -not -executable)

##########################################################################
find . -size 33c -user bandit7 -group bandit6
# or
find / -user bandit7 -group bandit6 -size 33c 2>/dev/null # 0=stdin 1=stdout 2=stderr

##########################################################################

grep 'millionth' data.txt
or
cat data.txt | grep 'millionth'
##########################################################################

sort data.txt | uniq -u
# or
cat data.txt | sort | uniq -u
# or
cat data.txt | sort | uniq -c | grep '\s1\s' # or grep -v/--invert-match 10
##########################################################################

cat data.txt | grep -aoh '=\s.*'
# or
strings data.txt | grep ===

#10#######################################################################
base64 -d data.txt

#11#######################################################################
cat ~/data.txt | tr 'A-Za-z' 'N-ZA-Mn-za-m'

# 11-12

#12#######################################################################
xxd -r # revert hexdump to binary
file # for get file tipes
# then tar -xvf / gzip / bzip2

#13#######################################################################
ssh bandit14@localhost -p 2220 -i sshkey.private
fGrHPx402xGC7U7rXKDaxiWFTOiF0ENq

#14#######################################################################
nc localhost 30000
jN2kgmIXJ6fShzhT2avhotn4Zcka6tnt

#15#######################################################################
# Connect to a server supporting TLS:
openssl s_client -connect example.com:443
openssl s_client -host example.com -port 443
JQttfApK4SeyHwDlI9SXGR50qclOAil1

#16#######################################################################
nc -z -v localhost 31000-32000 2>&1 | grep succeeded
# or
nmap localhost -p 31000-32000
# then
nmap localhost -p 31046,31518,... -sV -T4
openssl s_client -host localhost -port 31...

-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAvmOkuifmMg6HL2YPIOjon6iWfbp7c3jx34YkYWqUH57SUdyJ
imZzeyGC0gtZPGujUSxiJSWI/oTqexh+cAMTSMlOJf7+BrJObArnxd9Y7YT2bRPQ
Ja6Lzb558YW3FZl87ORiO+rW4LCDCNd2lUvLE/GL2GWyuKN0K5iCd5TbtJzEkQTu
DSt2mcNn4rhAL+JFr56o4T6z8WWAW18BR6yGrMq7Q/kALHYW3OekePQAzL0VUYbW
JGTi65CxbCnzc/w4+mqQyvmzpWtMAzJTzAzQxNbkR2MBGySxDLrjg0LWN6sK7wNX
x0YVztz/zbIkPjfkU1jHS+9EbVNj+D1XFOJuaQIDAQABAoIBABagpxpM1aoLWfvD
KHcj10nqcoBc4oE11aFYQwik7xfW+24pRNuDE6SFthOar69jp5RlLwD1NhPx3iBl
J9nOM8OJ0VToum43UOS8YxF8WwhXriYGnc1sskbwpXOUDc9uX4+UESzH22P29ovd
d8WErY0gPxun8pbJLmxkAtWNhpMvfe0050vk9TL5wqbu9AlbssgTcCXkMQnPw9nC
YNN6DDP2lbcBrvgT9YCNL6C+ZKufD52yOQ9qOkwFTEQpjtF4uNtJom+asvlpmS8A
vLY9r60wYSvmZhNqBUrj7lyCtXMIu1kkd4w7F77k+DjHoAXyxcUp1DGL51sOmama
+TOWWgECgYEA8JtPxP0GRJ+IQkX262jM3dEIkza8ky5moIwUqYdsx0NxHgRRhORT
8c8hAuRBb2G82so8vUHk/fur85OEfc9TncnCY2crpoqsghifKLxrLgtT+qDpfZnx
SatLdt8GfQ85yA7hnWWJ2MxF3NaeSDm75Lsm+tBbAiyc9P2jGRNtMSkCgYEAypHd
HCctNi/FwjulhttFx/rHYKhLidZDFYeiE/v45bN4yFm8x7R/b0iE7KaszX+Exdvt
SghaTdcG0Knyw1bpJVyusavPzpaJMjdJ6tcFhVAbAjm7enCIvGCSx+X3l5SiWg0A
R57hJglezIiVjv3aGwHwvlZvtszK6zV6oXFAu0ECgYAbjo46T4hyP5tJi93V5HDi
Ttiek7xRVxUl+iU7rWkGAXFpMLFteQEsRr7PJ/lemmEY5eTDAFMLy9FL2m9oQWCg
R8VdwSk8r9FGLS+9aKcV5PI/WEKlwgXinB3OhYimtiG2Cg5JCqIZFHxD6MjEGOiu
L8ktHMPvodBwNsSBULpG0QKBgBAplTfC1HOnWiMGOU3KPwYWt0O6CdTkmJOmL8Ni
blh9elyZ9FsGxsgtRBXRsqXuz7wtsQAgLHxbdLq/ZJQ7YfzOKU4ZxEnabvXnvWkU
YOdjHdSOoKvDQNWu6ucyLRAWFuISeXw9a/9p7ftpxm0TSgyvmfLF2MIAEwyzRqaM
77pBAoGAMmjmIJdjp+Ez8duyn3ieo36yrttF5NSsJLAbxFpdlc1gvtGCWW+9Cq0b
dxviW8+TFVEBl1O4f7HVm6EpTscdDxU+bCXWkfjuRb7Dy9GOtt9JPsX8MBTakzh3
vBgsyi/sN3RqRBcGU40fOoZyfAMT8s1m/uYv52O6IgeuZ/ujbjY=
-----END RSA PRIVATE KEY-----

#17#######################################################################
chmod go-rwx privatekey_file
ssl -i privatekey_file bandit17@localhost -p 2220

VwOSWtCA7lRKkTfbr2IDh6awj9RNZM5e
#18#######################################################################
diff pass.old pass.new

hga5tuuCLF6fFzUpnagiMN8ssu9LFrdg
#19#######################################################################
sshpass -p hga5tuuCLF6fFzUpnagiMN8ssu9LFrdg ssh bandit18@bandit.labs.overthewire.org -p 2220 cat readme

awhqfNnAbc1naukrpqDYcF95h7HoMTrC
#20#######################################################################
./bandit20-do cat /etc/bandit_pass/bandit20

VxCazJaVykI6W36BkBU0mJTCM8rR95XT
#21#######################################################################
nc -l localhost 2222
./succonect 2222

NvEJF7oVjkddltPSrdKEFOllh9V1IBcq

#22#######################################################################
cat cronjob_bandit22
@reboot bandit22 /usr/bin/cronjob_bandit22.sh &> /dev/null
* * * * * bandit22 /usr/bin/cronjob_bandit22.sh &> /dev/null

/usr/bin$ ./cronjob_bandit22.sh

cat t7O6lds9S0RqQh9aMcz6ShpAoZKF7fgv
WdDozAdTM2z9DiFEQ2mGlwngMfj4EZff

#22#######################################################################
/etc/cron.d$ cat cronjob_bandit23
/etc/cron.d$ cd /usr/bin/
/usr/bin$ cat cronjob_bandit23.sh
/tmp/8169b67bd894ddbb4412f91573b38db3

WdDozAdTM2z9DiFEQ2mGlwngMfj4EZff
#23#######################################################################
# EXAMPLE
# cat your.sh
#!/bin/bash

# myname=bandit23
# mytarget=$(echo I am user $myname | md5sum | cut -d ' ' -f 1)
# echo "Copying passwordfile /etc/bandit_pass/$myname to /tmp/$mytarget"
# cat /etc/bandit_pass/$myname > /tmp/$mytarget

# or
myname=bandit23
echo I am user $myname | md5sum | cut -d ' ' -f 1


cat /tmp/8ca319486bfbbc3663ea0fbe81326349

QYw0Y2aiA672PsMmh9puTQuhoz8SyR2G

#23>24#######################################################################
cat /etc/cron.d/cronjob_bandit24
# ls -la /usr/bin/ | grep cronjob
cat /usr/bin/cronjob_bandit24.sh

mkdir idaprvd
chmod 777 idaprvd

touch pass.sh
#!/bin/bash
cat /etc/bandit_pass/bandit24 > /tmp/idaprvd/pswrd

chmod 777 pass.sh

# then
cp pass.sh /var/spool/bandit24/foo/

VAfGXJ1PBSsPSnvsjI8p759leLZ9GGar

#24>25#######################################################################
for i in {0000..9999}; do echo VAfGXJ1PBSsPSnvsjI8p759leLZ9GGar $i; done | nc localhost 30002

p7TaowMYrmu23Ol8hiZh9UvD0O9hpx8d
#25>26#######################################################################
cat /etc/passwd | grep bandit26
cat /usr/bin/showtext
more text.txt
use `v` to enter vim
:e or :r /etc/bandit_pass/bandit26

c7GvcKlw9mC7aUQaPx7nwFstuAIBw1o1

# enter to enother shell
:set shell=/bin/bash
:shell

#26>27#######################################################################

./bandit27-do cat /etc/bandit_pass/bandit27

YnQpBuifNMas1hcUFk70ZmqkhUU2EuaS

#27>28#######################################################################
mktemp -d

git clone ssh://bandit27-git@localhost:2220/home/bandit27-git/repo

AVanL161y9rsbcJIsFHuw35rjaOM19nR
#28>29#######################################################################
git log
git show <commit>

+- username: bandit29
+- password: <TBD>

tQKvmcwNYcFS6vmPHIUSI3ShmsrQZK8S

#29>30#######################################################################
git branch -a
git checkout dev

xbhV3HpNGlTIdnjUrdAlPzc2L6y9EOnS
#30>31#######################################################################
git tag
git show secret

OoffzGDlzhAlerFJ2cAiz1D41JW1Mhmt
#31>32#######################################################################
touch key.txt
"May I come in?"
# remove from .gitignore *.txt
ga && gcm "" & gp

rmCBvG56y58BXzv98yZGdO7ATVL5dW8y
#32>33#######################################################################
$0
cat /etc/bandit_pass/bandit33

odHo63fHiFqcWWJG9rLiLDtPm45KzUK
odHo63fHiFqcWWJG9rLiLDtPm45KzUKy

#33>34#######################################################################
Congratulations on solving the last level of this game!
```
