# CTF Challenges Repository

This repository contains notes, files, and solutions from various Capture The Flag (CTF) challenges across multiple platforms. It is designed to document my progress, findings, and learnings while solving these challenges, covering topics such as reverse engineering, binary exploitation, web application security, and more.

>[!attention]
> Concentrate on [#%5BOverTheWire%5D(./OverTheWire/OverTheWire.md)] and [[#%5BPortSwigger - Web Security Academy%5D(./PortSwigger/WebSecAcademy.md)]]

## Directory Structure

### HackTheBox
##### [HackTheBox Machines](./HackTheBox/HTBMachines.md):
##### [HackTheBox Challenges](./HackTheBox/Challenges.md):
- Notes and solutions for HackTheBox machines.

#### CTF's
- [CaptureThePresent](./HackTheBox/CTF/CaptureThePresent.md)


### [OverTheWire](./OverTheWire/OverTheWire.md)
- Solutions and walkthroughs for OverTheWire challenges.

### [PicoCTF](./PicoCTF/PicoCTF.md)
- General notes on picoCTF challenges.

### [THM Machines](./TryHackMe/THMMachines.md)
- Documentation of machines solved on TryHackMe.

### [PortSwigger - Web Security Academy](./PortSwigger/WebSecAcademy.md)

- **Vulnhub**:
  - [Vulnhub Machines List](./VulnHub/vulnhubmachineslist.md): Notes and solutions for machines hosted on VulnHub.

### [DVWA](./DVWA/DVWA.md)
- Notes and solutions for DVWA challenges.

## Resources

- [awesome-ctf](https://github.com/apsdehal/awesome-ctf?tab=readme-ov-file)
- [wargame-nexus](https://github.com/zardus/wargame-nexus)

## **Other**:

  - [Command Line Challenge](./other/cmdchallenge.com.md): Notes from [cmdchallenge.com](https://cmdchallenge.com)
  - [UnderTheWire](./other/UnderTheWire.md): Solutions and write-ups for challenges from UnderTheWire.
  - [Holiday Hack Challenge](./other/.md): Solutions and write-ups for challenges from Holiday Hack Challenge.

## Utility Scripts

- [Clean Temp Folders](scripts/cleantemp.sh)
- [convert_hex32bit.py](scripts/convert_hex32bit.py): A Python script to convert hexadecimal values to 32-bit format.


## Purpose

This repository serves as a learning resource and a personal archive for CTF challenges, covering a variety of techniques and tools used in different security challenges. It’s intended for educational purposes and to share insights into solving these challenges.

Feel free to explore the content and use the files for learning and improving your own skills.

Happy hacking! (..◜ᴗ◝..)
