[Back](../README.md)

# PicoCTFs

This directory contains various challenges and solutions for PicoCTF competitions. The challenges are organized by category and include detailed notes and solutions.

## Table of Contents

## [General Skills Category](GeneralSkills.md)
## [Web Exploitation Category](WebExploitation.md)
## [Reverse Engineering Category](ReverseEngineering.md)
## [Cryptography Category](Cryptography.md)
## [Forensics Category](Forensics.md)

___

## [ARMssembly Notes](assemblypicoctf/ARMssembly.md)

## Binary Search
- [Binary Search Challenge File](binarysearch/guessing_game.sh)

___
