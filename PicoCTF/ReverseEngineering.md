[Back](PicoCTF.md)

>[!note] Only the challenges that were interesting to me.

# Reverse Engineering Category

## Easy Difficulty

### [Transformation](https://play.picoctf.org/practice/challenge/104)
- [decription](reverse/transformation.py)

## Medium Difficulty

### [Bbbbloat](https://play.picoctf.org/practice/challenge/255)
- *Ghidra* analysis: Decompile tab -> Search for `main` *ifelse* statement -> convert number to decimal
- `echo <magic_number> | ./bbbbloat | grep -oE "picoCTF{.*?}" --color=none`

### [format string 0](https://play.picoctf.org/practice/challenge/433)

>[!hint] Format Specifiers
> When printf encounters %114d, it expects to print a number with a width of 114 characters. Since no number is provided, it can print some garbage data, inflating the count to 114 characters.
> When printf processes %s without a corresponding string argument, it can access arbitrary memory or cause the program to crash.
