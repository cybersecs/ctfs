[Back](PicoCTF.md)

>[!note] Only the challenges that were interesting to me.

# Cryptography Category

## Easy Difficulty

### [interencdec](https://play.picoctf.org/practice/challenge/418)
- use [CyberChef](https://gchq.github.io/CyberChef/) to decrypt
- base64 decode
- rot13 decode (19) or bruteforce

### [The Numbers](https://play.picoctf.org/practice/challenge/68)
>[!hint]
> - Google 'alphabet cipher number to letter'

