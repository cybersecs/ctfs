def transform (enc):
    flag = ""
    for i in range(len(enc)):
        flag += chr(ord(enc[i]) >> 8)
        flag += chr(ord(enc[i]) - (ord(flag[-1]) << 8))
    return flag

if __name__ == '__main__':
    with open('enc') as f:
        readed = f.read()
        decripted = transform(readed)
        print(decripted)
        # or just

        print(readed.encode('utf-16be'))
