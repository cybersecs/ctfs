[Back](PicoCTF.md)

>[!note] Only the challenges that were interesting to me.

# Forensics Category

## Easy Difficulty

### [Verify](https://play.picoctf.org/practice/challenge/450)

>[!hint] Oneliner
> `sha256sum files/* | grep "$(cat checksum.txt)" | awk '{print $2}' | xargs ./decrypt.sh`

