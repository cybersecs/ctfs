[Back](PicoCTF.md)

>[!note] Only the challenges that were interesting to me.

# General Skills Category

## Easy Difficulty

### [repetitions](hhttps://play.picoctf.org/practice/challenge/371)

#### base64 decoding

```bash
base64 -d enc_flag | base64 -d | base64 -d | base64 -d | base64 -d | base64 -d
```

### [Big Zip](https://play.picoctf.org/practice/challenge/322): unzip and grep picoCTF flag

### [First Find](https://play.picoctf.org/practice/challenge/320): find picoCTF flag

```bash
find . -type f -name "*uber*"
# or just use grep (like in Big Zip)
```

### [HashingJobApp](https://play.picoctf.org/practice/challenge/243)

```bash
echo -n <words> | md5sum
# '-n' do not output newline
```

### [Lets Warm Up](https://play.picoctf.org/practice/challenge/22)

```bash
printf "\x<number>\n"
# prints the character corresponding to the hexadecimal value
```

### [Nice netcat...](https://play.picoctf.org/practice/challenge/156)

```bash
while read -r num; do printf "\x$num\n"; done < ncat.txt |
```

### [Glitch Cat](https://play.picoctf.org/practice/challenge/242)
```bash
# for extracting only glitched characters
cat <file_from_nc> | grep -oE "chr\(0x[0-9A-Fa-f]+\)?" | tr '\n' ' '
```

___

## Series of Challenges

### [PW Crack 1](https://play.picoctf.org/practice/challenge/245)
### [PW Crack 2](https://play.picoctf.org/practice/challenge/246)

>[!hint] password for 1-2 challenges in if condition (just print it)

### [PW Crack 3](https://play.picoctf.org/practice/challenge/247)
- [Modified checker](pwcrack/level3.py)

### [PW Crack 4](https://play.picoctf.org/practice/challenge/248)
- [Modified checker](pwcrack/level4.py)

### [PW Crack 4](https://play.picoctf.org/practice/challenge/249)
- [Modified checker](pwcrack/level5.py)



___

## Medium Difficulty

>[!todo]
> - [-] [SansAlpha](https://play.picoctf.org/practice/challenge/436)

### [mus1c](https://play.picoctf.org/practice/challenge/15)

>[!hint] Rockstar programming language
> [Online Compiler](https://codewithrockstar.com/online)
> Then use [ASCII Decoder](../scripts/decodeascii.c)


### [Based](https://play.picoctf.org/practice/challenge/35)
- just use [dataconverter](../scripts/dataconverter.py)

### [flag_shop](https://play.picoctf.org/practice/challenge/49)

>[!hint] Check
> [integer limits in c](../scripts/limits.c)

### [dont-you-love-banners](https://play.picoctf.org/practice/challenge/437)

- [DEFCON](https://defcon.org/)
- [Capitan Crunch](https://en.wikipedia.org/wiki/John_Draper)
- From description - find flag in `/root/flag.txt` and `ln -s` to home.


