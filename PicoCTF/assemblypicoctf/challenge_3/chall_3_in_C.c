#include <stdio.h>
#include <stdlib.h>

// Function equivalent to 'func2' in assembly
int func2(int arg) {
    return arg + 3;
}

// Function equivalent to 'func1' in assembly
int func1(int arg) {
    int var1 = 0;  // Equivalent to [x29+44]

    while (arg != 0) {
        if (arg & 1) {  // Check if the least significant bit is 1 (odd number)
            var1 = func2(var1);
        }
        arg >>= 1;  // Right shift arg by 1 (divide by 2)
    }

    return var1;
}

// Main function
int main(int argc, char *argv[]) {
    if (argc < 2) {
        fprintf(stderr, "Usage: %s <number>\n", argv[0]);
        return 1;
    }

    int num = atoi(argv[1]);  // Convert the command-line argument to an integer
    int result = func1(num);  // Call the 'func1' function

    printf("Result: %d\n", result);  // Print the result

    return 0;
}
