---
aliases:
- PicoCTF ARMssembly Notes
---

[CTFtime.org / picoCTF 2021 / ARMssembly-2 / Writeup](https://ctftime.org/writeup/26964)

## Table of Contents

- [x] [ARMssembly 0](#armssembly-0)
- [x] [ARMssembly 1](#armssembly-1)
- [-] [ARMssembly 2](#armssembly-2)

- [Back](../PicoCTF.md)

## ARMssembly 0

- [chall_1.S](assemblypicoctf/challenge_1/chall_1.S): The assembly code for the challenge.
- [chall_1_commented.S](assemblypicoctf/challenge_1/chall_1_commented.S): The commented version of the challenge file, explaining each step.
- [chall_1](challenge_1/chall_1): Binary file.

Install the GNU Assembler (GAS) and GNU Linker (LD) if you haven't already.

```assembly
.arch armv8-a
```

Install a cross-compiler or cross-assembler that supports the ARMv8 architecture - `aarch64-linux-gnu-gcc`.
A cross-compiler or cross-assembler is a toolchain that can create executables for a different architecture than the one it is running on.

```bash
aarch64-linux-gnu-gcc -o chall chall.S -static
```

Or run the executable file on an emulated ARMv8-based system using [qemu-aarch64](https://wiki.archlinux.org/title/QEMU)

```bash
# https://wiki.archlinux.org/title/QEMU
qemu-aarch64 -L /usr/aarch64-linux-gnu chall
qemu-aarch64 chall <number> <number> | xargs printf '%x\n'
```

[CTFtime.org / picoCTF 2021 / ARMssembly-1 / Writeup](https://ctftime.org/writeup/26963)

## ARMssembly-1

### Files

- [chall_2.S](assemblypicoctf/challenge_2/chall_2.S): The assembly code for the challenge.
- [chall_2_commented.S](assemblypicoctf/challenge_2/chall_2_commented.S): The commented version of the challenge file, explaining each step.
- [chall_2_in_C.c](assemblypicoctf/challenge_2/chall_2_in_C.c): The C version of the challenge for better understanding.
- [chall_2](assemblypicoctf/challenge_2/chall_2): Binary file.


Let's go through the code step by step, explaining what each line does.

**Section 1: ARMv8-A Assembly Code (func function)**

```assembly
.arch armv8-a
.file	"chall_1.c"
.text
.align	2
.global	func
.type	func, %function
```

- `.arch armv8-a`: Specifies the instruction set architecture as ARMv8-A.
- `.file "chall_1.c"`: Identifies the source file name ("chall_1.c") associated with this assembly code.
- `.text`: Indicates that the following instructions are part of the code segment (i.e., executable instructions).
- `.align 2`: Aligns the next instruction to a memory address that is a multiple of 4, because we're on an ARMv8-A architecture.
- `.global func`: Declares that the `func` function should be globally accessible from outside this file.
- `.type func, %function`: Specifies the type of `func` as a function.

```assembly
func:
	sub	sp, sp, #32
	str	w0, [sp, 12]
	mov	w0, 85
	str	w0, [sp, 16]
	mov	w0, 6
	str	w0, [sp, 20]
	mov	w0, 3
	str	w0, [sp, 24]
```

- `sub sp, sp, #32`: Decrements the stack pointer (`sp`) by 32 bytes to allocate space on the stack. This is a common way in ARM assembly to create local variables or space for function calls.
- `str w0, [sp, 12]`: Stores the value of register `w0` into memory at the address specified by `sp + 12`. Essentially, this sets aside some initial data or parameters on the stack.
- The following lines store different values (85, 6, and 3) onto the stack in consecutive locations.

```assembly
ldr	w0, [sp, 20]
ldr	w1, [sp, 16]
	lsl	w0, w1, w0
	str	w0, [sp, 28]
```

- `ldr w0, [sp, 20]`: Loads the value stored at address `sp + 20` into register `w0`.
- `ldr w1, [sp, 16]`: Loads the value stored at address `sp + 16` into register `w1`.
- `lsl w0, w1, w0`: Shifts the bits of the value in `w1` left by the number of places specified in `w0`, effectively multiplying `w1` by the power of 2 represented by `w0`. This is a form of multiplication.
- `str w0, [sp, 28]`: Stores the result of the multiplication into memory at address `sp + 28`.

```assembly
ldr	w1, [sp, 28]
ldr	w0, [sp, 24]
	sdiv	w0, w1, w0
	str	w0, [sp, 28]
```

- `ldr w1, [sp, 28]`: Loads the result of a previous operation (or value stored) into register `w1`.
- `ldr w0, [sp, 24]`: Loads another previously stored value into register `w0`.
- `sdiv w0, w1, w0`: Performs division between `w1` and `w0`, storing the result in `w0`. This operation is a form of signed division.
- `str w0, [sp, 28]`: Stores the result of the division into memory at address `sp + 28`.

```assembly
ldr	w1, [sp, 28]
ldr	w0, [sp, 12]
	sub	w0, w1, w0
	str	w0, [sp, 28]
```

- Loads the value stored in `sp + 28` into register `w1`.
- Loads another previously stored value into register `w0`.
- Subtracts `w0` from `w1`, storing the result back in `w0`.
- Stores this result into memory at address `sp + 28`.

```assembly
ldr	w0, [sp, 28]
add	sp, sp, 32
	ret
```

- Loads the final value stored on the stack into register `w0`.
- Adds 32 to the current stack pointer (`sp`) to effectively remove the space allocated for local variables.
- `ret`: Returns from the function.

**Section 2: Main Function (main function)**

```assembly
main:
	sub	sp, sp, #8
	str	lr, [sp, #4]
	b	.start
```

- This part of the code is not shown in the provided snippet but it's common for initialization or setup of the stack frame.

```assembly
_start:
	ldr	r0, =func
	bl	func
	mov	r1, r0
	str	r1, [sp, #4]
	bl	main
	b	.end
```

- `ldr r0, =func`: Loads the address of the function `func` into register `r0`.
- `bl func`: Branches to the address in `r0`, calling the `func` function.
- `mov r1, r0`: Moves the return value of `func` (stored in `r0`) into register `r1`. Typically used for storing results or passing back data from a function.
- `str r1, [sp, #4]`: Stores this return value into memory at address `sp + 4`.
- `bl main`: Branches to the address of `main` again. In a typical program flow, you wouldn't expect to call `main` recursively, so it's unusual here.
- `b .end`: Branches unconditionally to the label `.end`, which is not shown in this snippet but typically marks the end of the program.

```assembly
.end:
	lr	[sp], #4
	add	sp, sp, #8
	b	.end
```

- Loads from memory at address `sp + 4` into the Link Register (`lr`). This can be used to return control flow in certain situations.
- Adds 8 to the stack pointer (`sp`) to deallocate the space allocated for local variables and parameters.
- Branches back to `.end`, which is not shown but likely marks the end of the program.

```assembly
.size	main, .-main
.ident	"GCC: (Ubuntu/Linaro 7.5.0-3ubuntu1~18.04) 7.5.0"
```

- `.size main`: Specifies the size of the `main` function.
- `.ident "GCC version"`: Provides information about the compiler used to generate this assembly code.

## ARMssembly 2

### Files:

- [chall_3.S](assemblypicoctf/challenge_3/chall_3.S): The assembly code for the challenge.

