#include <stdio.h>
#include <stdlib.h>

// Function equivalent to 'func1' in assembly
unsigned int func1(unsigned int arg) {
    unsigned int var1 = 0;
    unsigned int var2 = 0; // Initialize var2 to 0 to match assembly logic

    while (var2 < arg) { // Use < to match 'bcc' (less than comparison) in assembly
        var1 += 3;
        var2 += 1;
    }

    return var1;
}

// Main function
int main(int argc, char *argv[]) {
    if (argc < 2) {
        fprintf(stderr, "Usage: %s <number>\n", argv[0]);
        return 1;
    }

    unsigned long num = strtoul(argv[1], NULL, 10); // Use strtoul to safely convert string to unsigned long
    unsigned int result = func1((unsigned int)num); // Cast to unsigned int to simulate 32-bit integer

    printf("Result: %u\n", result); // Print the result as an unsigned int

    return 0;
}
