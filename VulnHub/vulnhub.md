[Back](../README.md)

## Configuring

1. Change machine settings to *Internal Network* and name it.
2. Create DHCP server:
```bash
vboxmanage dhcpserver add --network=<internal_network_name> --server-ip=10.32.1.1 --lower-ip=10.32.1.123 --upper-ip=10.32.1.132 --netmask=255.255.255.0 --enable
```

# Concise list of some popular *machines grouped by difficulty*:

## Beginner:

1. [[kioptrix]]:
   - One of the most popular starting points
   - Focuses on basic enumeration and common vulnerabilities

2. Basic Pentesting: 1
   - Designed for beginners to learn basic penetration testing techniques
   - Covers enumeration, brute forcing, and privilege escalation

3. Metasploitable 2
   - Intentionally vulnerable Linux-based machine
   - Great for practicing with Metasploit framework

4. DVWA (Damn Vulnerable Web Application)
   - Web application with various vulnerabilities to exploit
   - Allows practice on common web vulnerabilities like SQL injection and XSS

5. LAMPSecurity: CTF4
   - Part of the LAMPSecurity series
   - Focuses on web application vulnerabilities

6. Tr0ll: 1
   - Beginner-friendly machine with a playful theme
   - Teaches basic enumeration and exploitation techniques

7. Bulldog: 1
   - Focuses on web application vulnerabilities and basic privilege escalation
   - Good for practicing OSCP-like techniques

8. Manpage
   - Simple machine focusing on Linux privilege escalation
   - Good for learning about misconfigurations in Linux systems

9. Lazysysadmin: 1
   - Simulates a poorly configured system
   - Teaches the importance of proper system administration

10. DC: 1
    - First in the DC series, designed for beginners
    - Focuses on basic web application vulnerabilities and simple privilege escalation

11. Toppo: 1
    - Simple machine for beginners
    - Teaches basic enumeration and exploitation techniques

12. Pwnlab: Init
    - Beginner-friendly machine focusing on web vulnerabilities
    - Includes file inclusion and SQL injection challenges

These machines cover a range of basic security concepts and vulnerabilities, making them ideal for beginners to practice their skills. They typically involve techniques such as:

- Basic network enumeration
- Web application vulnerability exploitation
- Simple privilege escalation
- Password cracking
- Common misconfigurations

## Intermediate:

1. Kioptrix (Level 2-4)
2. Mr-Robot: 1
3. HackInOS: 1
4. Vulnix

## Advanced:

1. Kioptrix: 2014
2. SkyTower: 1
3. PwnLab: init
4. Brainpan: 1

## Expert:

1. HackLAB: Vulnix
2. Acid: Reloaded
3. Hackfest2016: Sedna
4. Wintermute: 1
