#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Function to display help message
void print_help() {
    printf("Usage: ./decode_ascii [file]\n");
    printf("Decode ASCII or extended ASCII values into characters.\n");
    printf("If a file is provided, it reads ASCII values from the file.\n");
    printf("Otherwise, it takes user input from the console.\n");
    printf("\nOptions:\n");
    printf("  -h, --help     Show this help message and exit\n");
}

// Function to decode ASCII values from a file
void decode_ascii_file(const char *filename) {
    FILE *file = fopen(filename, "r");  // Open file for reading
    if (file == NULL) {
        perror("Error opening file");
        exit(EXIT_FAILURE);
    }

    int ascii_value;
    while (fscanf(file, "%d", &ascii_value) == 1) {  // Read each integer value from file
        printf("%c", (unsigned char)ascii_value);  // Convert to character (supports extended ASCII)
    }
    printf("\n");

    fclose(file);  // Close the file
}

// Function to decode ASCII values from user input
void decode_ascii_input() {
    char input[256];
    printf("Enter ASCII values separated by spaces: ");
    fgets(input, sizeof(input), stdin);  // Get user input

    char *token = strtok(input, " ");
    while (token != NULL) {
        int ascii_value = atoi(token);  // Convert the string token to an integer
        printf("%c", (unsigned char)ascii_value);  // Print corresponding ASCII character
        token = strtok(NULL, " ");  // Get the next token
    }
    printf("\n");
}

int main(int argc, char *argv[]) {
    if (argc == 2) {
        if (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0) {
            print_help();  // Show help message
            return EXIT_SUCCESS;
        } else {
            decode_ascii_file(argv[1]);  // Decode ASCII values from file
        }
    } else if (argc == 1) {
        decode_ascii_input();  // Decode ASCII values from user input
    } else {
        fprintf(stderr, "Invalid number of arguments. Use -h or --help for usage information.\n");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
