#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

// Function to display help message
void print_help() {
    printf("Usage: ./binary_to_ascii [file]\n");
    printf("Convert binary input to ASCII.\n");
    printf("If a file is provided, it reads the binary from the file.\n");
    printf("Otherwise, it takes user input from the console.\n");
    printf("\nOptions:\n");
    printf("  -h, --help     Show this help message and exit\n");
}

// Function to convert binary string to ASCII
char binary_to_ascii(const char *binary_str) {
    int ascii_value = 0;
    for (int i = 0; i < 8; i++) {
        ascii_value = ascii_value * 2 + (binary_str[i] - '0');
    }
    return (char)ascii_value;
}

// Function to process binary string from a file or input
void process_binary_string(const char *binary_str) {
    size_t len = strlen(binary_str);
    if (len % 8 != 0) {
        fprintf(stderr, "Error: Input binary string must be a multiple of 8 bits.\n");
        exit(EXIT_FAILURE);
    }

    for (size_t i = 0; i < len; i += 8) {
        char ascii_char = binary_to_ascii(&binary_str[i]);
        printf("%c", ascii_char);
    }
    printf("\n");
}

// Function to handle binary input from file
void process_file(const char *filename) {
    FILE *file = fopen(filename, "r");
    if (!file) {
        perror("Error opening file");
        exit(EXIT_FAILURE);
    }

    char binary_str[9]; // buffer to hold binary chunks
    while (fscanf(file, "%8s", binary_str) == 1) {
        printf("%c", binary_to_ascii(binary_str));
    }
    printf("\n");

    fclose(file);
}

int main(int argc, char *argv[]) {
    if (argc == 2) {
        if (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0) {
            print_help();
            return EXIT_SUCCESS;
        } else {
            process_file(argv[1]);
        }
    } else if (argc == 1) {
        char input[256];
        printf("Enter a binary string: ");
        fgets(input, sizeof(input), stdin);
        input[strcspn(input, "\n")] = 0; // Remove the newline character
        process_binary_string(input);
    } else {
        fprintf(stderr, "Invalid number of arguments. Use -h or --help for usage information.\n");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

