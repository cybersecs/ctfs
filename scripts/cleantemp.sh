#!/bin/bash

# Find all directories named "temp" and iterate over them
find ../ -type d -name "temp" | while read -r temp_dir; do
    echo "Cleaning contents of $temp_dir"

    # Remove all files and directories inside the temp directory
    rm -rf "$temp_dir"/*

    # Optional: Verify the folder is empty
    if [ -z "$(ls -A "$temp_dir")" ]; then
        echo "$temp_dir is now clean."
    else
        echo "Failed to clean $temp_dir."
    fi
done

echo "Cleaning completed."

