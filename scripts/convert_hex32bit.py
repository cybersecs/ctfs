import argparse


def to_32bit_hex(number):
    """
    Converts a given number to its 32-bit hexadecimal representation.

    Parameters:
        number (int): The number to be converted.

    Returns:
        str: The 32-bit hexadecimal representation of the input number.

    Use bitwise operators to convert the number to a 32-bit hexadecimal string
    """
    result = f"{number & 0xFFFFFFFF:08X}".lower()

    return result

# def to_32bit_hex(number):
#     # Convert the number to hexadecimal
#     hex_number = hex(number & 0xFFFFFFFF)[2:]  # Use bitwise AND to ensure it's 32-bit

#     # Zero-pad the hexadecimal string to ensure it's 8 characters long
#     hex_32bit = hex_number.zfill(8).upper()  # Convert to uppercase for consistency

#     return hex_32bit


if __name__ == "__main__":
    # Set up argument parsing
    parser = argparse.ArgumentParser(
        description="Convert a number to a 32-bit hexadecimal representation.")
    parser.add_argument("number", type=int,
                        help="The number to convert to 32-bit hex.")

    # Parse the arguments
    args = parser.parse_args()

    # Convert the number to 32-bit hex
    hex_32bit = to_32bit_hex(args.number)

    # Print the result
    print(f"32-bit Hexadecimal: {hex_32bit}")
