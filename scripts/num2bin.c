#include <stdio.h>

void decimalToBinary(int n) {
    // Array to store binary number
    int binaryNum[32];

    // Counter for binary array
    int i = 0;
    while (n > 0) {
        // Storing remainder when n is divided by 2
        binaryNum[i] = n % 2;
        n = n / 2;
        i++;
    }

    // Printing binary number in reverse order
    printf("Binary representation: ");
    for (int j = i - 1; j >= 0; j--) {
        printf("%d", binaryNum[j]);
    }
    printf("\n");
}

int main() {
    int n;

    // Input decimal number
    printf("Enter a decimal number: ");
    scanf("%d", &n);

    // Handling the case where input is 0
    if (n == 0) {
        printf("Binary representation: 0\n");
    } else {
        decimalToBinary(n);
    }

    return 0;
}
